# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information

import unittest
import setup_unittests  # Do not remove
from team import Team
from player import Player
from database import Database
from config import Config
import os


class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.test_players_1: list[Player] = [
            Player("Julia", points=10),
            Player("Imanuel"),
            Player("Jessy", points=15),
            Player("David")
        ]
        self.test_players_2: list[Player] = [
            Player("Tom", points=14),
            Player("Heinz"),
            Player("Manfred", points=36),
            Player("Johann")
        ]
        self.test_team_without_id: Team = Team("test_team_without_id", self.test_players_1)
        self.test_team_with_id: Team = Team("test_team_with_id", self.test_players_2, team_id=123)
        self.test_database = Database(test_mode_enabled=True)

    def tearDown(self):
        self.test_database.close()
        os.remove(Config.TEST_MODE_DATABASE_PATH)

    def test_saving_new_team_works_correctly(self):
        self.test_database.save_team(self.test_team_without_id)
        self.assertIsInstance(self.test_database.get(doc_id=1), dict)
        self.assertTrue("players" in self.test_database.get(doc_id=1).keys())
        self.assertEqual(len(self.test_database.get(doc_id=1)["players"]), 4)

    def test_saving_team_with_untracked_id_results_in_new_id(self):
        old_id: int = self.test_team_with_id.team_id
        self.test_database.save_team(self.test_team_with_id)
        new_id = self.test_team_with_id.team_id
        self.assertNotEqual(old_id, new_id)

    def test_loading_team_works(self):
        self.test_database.save_team(self.test_team_without_id)
        team_id: int = self.test_team_without_id.team_id

        loaded_team: Team = self.test_database.load_team(team_id)

        self.assertIsInstance(loaded_team, Team)
        self.assertEqual(len(loaded_team), 4)
        self.assertIsNotNone(loaded_team.get_player_by_name("Julia"))
        self.assertEqual(loaded_team.get_player_by_name("Julia").name, "Julia")

    def test_saving_existing_team_updates_data(self):
        self.test_database.save_team(self.test_team_without_id)
        team_id: int = self.test_team_without_id.team_id
        loaded_team: Team = self.test_database.load_team(team_id)
        self.assertEqual(loaded_team.get_player_by_name("Jessy").name, "Jessy")
        loaded_team.get_player_by_name("Jessy").name = "Jessman"
        self.test_database.save_team(loaded_team)
        loaded_team_2 = self.test_database.load_team(loaded_team.team_id)
        self.assertEqual(loaded_team_2.get_player_by_name("Jessman").name, "Jessman")

    def test_get_scoreboard_works_correctly(self):
        dummy_team_1: Team = Team("dummy_team_1", [Player("Dummy1", points=12), Player("Dummy2", points=6)])  # Sum: 18
        dummy_team_2: Team = Team("dummy_team_2", [Player("Dummy3", points=2), Player("Dummy4", points=15)])  # Sum: 17
        dummy_team_3: Team = Team("dummy_team_3", [Player("Dummy5", points=8), Player("Dummy6", points=90)])  # Sum: 98

        self.test_database.save_team(dummy_team_1)
        self.test_database.save_team(dummy_team_2)
        self.test_database.save_team(dummy_team_3)

        scoreboard: list[tuple] = self.test_database.get_scoreboard()
        self.assertEqual(scoreboard[0], ("dummy_team_3", 98))
        self.assertEqual(scoreboard[1], ("dummy_team_1", 18))
        self.assertEqual(scoreboard[2], ("dummy_team_2", 17))


if __name__ == '__main__':
    unittest.main()
