# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information

import unittest
import setup_unittests  # Do not remove
from player import Player


class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.test_player: Player = Player("TestName")

    def test_name_gets_initialized_correctly(self):
        self.assertEqual(self.test_player.name, "TestName")

    def test_changing_name_works_correctly(self):
        new_name: str = "SomethingDifferent"
        self.test_player.name = new_name
        self.assertEqual(self.test_player.name, new_name)

    def test_adding_points_works_correctly(self):
        points_to_add: int = 6
        self.test_player.add_points(points_to_add)
        self.assertEqual(self.test_player.points, points_to_add)

    def test_removing_points_works_correctly(self):
        initial_points: int = 100
        test_player_with_initial_points = Player("TestName", points=initial_points)
        points_to_remove: int = 12
        test_player_with_initial_points.remove_points(points_to_remove)
        self.assertEqual(test_player_with_initial_points.points, initial_points - points_to_remove)
        test_player_with_initial_points.remove_points(999999)  # Should not go negative
        self.assertEqual(test_player_with_initial_points.points, 0)

    def test_hard_setting_points_works_correctly(self):
        hard_set_points: int = 1234
        self.test_player.points = hard_set_points
        self.assertEqual(self.test_player.points, hard_set_points)


if __name__ == '__main__':
    unittest.main()
