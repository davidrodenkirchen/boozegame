# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information

import unittest
import setup_unittests  # Do not remove
from team import Team, TeamHasDuplicateNamesException
from player import Player


class TestTeam(unittest.TestCase):
    def setUp(self):
        players: list[Player] = [
            Player("Julia", points=10),
            Player("Imanuel"),
            Player("Jessy", points=15),
            Player("David")
        ]
        self.test_team: Team = Team("test_team", players)

    def test_single_player_team_initializes_correctly(self):
        player: Player = Player("SinglePlayer")
        single_player_team: Team = Team("single_player_team", player)
        self.assertIsInstance(single_player_team, Team)
        self.assertEqual(len(single_player_team), 1)

    def test_points_get_calculated_correctly(self):
        self.assertEqual(self.test_team.points, 25)

    def test_getting_player_by_name_returns_correct_player(self):
        jessy = self.test_team.get_player_by_name("Jessy")
        self.assertEqual(jessy.points, 15)
        self.assertEqual(jessy.name, "Jessy")

    def test_getting_player_by_name_returns_none_if_player_does_not_exist(self):
        self.assertIsNone(self.test_team.get_player_by_name("Markus"))

    def test_modifying_player_saves_correctly(self):
        imanuel = self.test_team.get_player_by_name("Imanuel")
        self.assertEqual(self.test_team.points, 25)
        imanuel.add_points(5)
        self.assertEqual(self.test_team.points, 30)

    def test_team_with_duplicate_names_raises_exception(self):
        players: list[Player] = [
            Player("Thomas"),
            Player("Manfred"),
            Player("Thomas"),
        ]
        self.assertRaises(TeamHasDuplicateNamesException, lambda: Team("x", players))

    def test_adding_player_with_duplicate_name_raises_exception(self):
        self.assertRaises(TeamHasDuplicateNamesException, lambda: self.test_team.add_player(Player("Julia")))

    def test_adding_player_works_correctly(self):
        new_player = Player("Hans-Werner")
        self.test_team.add_player(new_player)
        self.assertEqual(len(self.test_team), 5)
        self.assertIsInstance(self.test_team.get_player_by_name("Hans-Werner"), Player)

    def test_removing_existing_player_by_name_works_correctly(self):
        self.test_team.remove_player("Imanuel")
        self.assertEqual(len(self.test_team), 3)
        self.assertIsNone(self.test_team.get_player_by_name("Imanuel"))

    def test_removing_non_existing_player_by_name_does_not_raise_exception(self):
        self.test_team.remove_player("Peter Mustermann")
        self.assertEqual(len(self.test_team), 4)

    def test_removing_existing_player_by_reference_works_correctly(self):
        test_player: Player = Player("Martin")

        self.test_team.add_player(test_player)
        self.assertEqual(len(self.test_team), 5)
        self.assertIsInstance(self.test_team.get_player_by_name("Martin"), Player)

        self.test_team.remove_player("Martin")
        self.assertEqual(len(self.test_team), 4)
        self.assertIsNone(self.test_team.get_player_by_name("Martin"))


if __name__ == '__main__':
    unittest.main()
