#!/bin/bash

cd ./testing || return 1
python3 -m unittest unittests_player.TestPlayer
python3 -m unittest unittests_team.TestTeam
python3 -m unittest unittests_database.TestDatabase