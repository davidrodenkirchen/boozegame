# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information
import hashlib
import secrets
from typing import Optional

import tinydb
from tinydb import where

from config import Config as globalConfig
from flask import Flask, request, render_template, g, session, flash
from flask_babel import Babel, gettext
from flask_login import LoginManager, login_user, current_user, logout_user
from player import Player
from team import Team
from database import Database

boozegame_server = Flask(__name__, static_folder="static")
babel = Babel(boozegame_server)
login_manager = LoginManager()
login_manager.init_app(boozegame_server)
boozegame_server.secret_key = secrets.token_urlsafe(32)
boozegame_server.config["LANGUAGES"] = globalConfig.LANGUAGES
boozegame_server.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0


@babel.localeselector
def get_locale():
    # return "de"  # For translation testing
    return request.accept_languages.best_match(boozegame_server.config["LANGUAGES"])


@login_manager.user_loader
def user_loader(team_id: str) -> Optional[Team]:
    try:
        return global_team_register[int(team_id)]
    except KeyError:
        return


# ToDo: Admin-Account required for this site
@boozegame_server.route("/debug", methods=["GET"])
def debug():
    return render_template("component.html")


@boozegame_server.route("/", methods=["GET"])
def landing():
    g.active_menu_item = "home"
    return render_template("home.html")


@boozegame_server.route("/home", methods=["GET"])
def home():
    g.active_menu_item = "home"
    return render_template("home.html")


@boozegame_server.route("/contact", methods=["GET"])
def contact():
    g.active_menu_item = "contact"
    return render_template("contact.html")


@boozegame_server.route("/play", methods=["GET"])
def play():
    g.active_menu_item = "play"
    return render_template("play.html")


@boozegame_server.route("/manageplayers", methods=["GET"])
def manageplayers():
    g.active_menu_item = "manageplayers"
    return render_template("manageplayers.html")


@boozegame_server.route("/privacy", methods=["GET"])
def privacy():
    g.active_menu_item = "privacy"
    return render_template("privacy.html")


@boozegame_server.route("/scoreboard", methods=["GET"])
def scoreboard():
    g.active_menu_item = "scoreboard"
    return render_template("scoreboard.html")


@boozegame_server.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template("login.html")
    elif request.method == "POST":
        team_name = str(request.form.get("team-name")).strip()
        password = str(request.form.get("password")).strip()
        with Database() as db:
            result = db.search(where("name") == team_name)
            if not result:  # Empty return
                return render_template("login.html", team_not_found=True)
            team = db.load_team(result[0].doc_id)
            if team.password == hashlib.sha256(password.encode("utf-8")).hexdigest():
                login_user(team)
                global_team_register[team.team_id] = team
            else:
                return render_template("login.html", password_incorrect=True)
        return render_template("home.html")


@boozegame_server.route("/register", methods=["GET", "POST"])
def register_team():
    if request.method == "GET":
        return render_template("registerteam.html")
    elif request.method == "POST":
        team_name = str(request.form.get("team-name")).strip()
        password = str(request.form.get("password")).strip()
        password_check = str(request.form.get("password-check")).strip()
        players = str(request.form.get("players")).strip().split(",")
        if password != password_check:
            return render_template("registerteam.html", passwords_not_identical=True)
        if len(password) < 3 or len(password) > 20:
            return render_template("registerteam.html", password_size_incorrect=True)
        with Database() as db:
            result = db.search(where("name") == team_name)
            if result:  # A team already exists with that name
                return render_template("registerteam.html", team_already_exists=True)
            players = [Player(name) for name in players]
            new_team = Team(team_name, players, password=hashlib.sha256(password.encode("utf-8")).hexdigest())
            db.save_team(new_team)
            login_user(new_team)
            global_team_register[new_team.team_id] = new_team
        return render_template("home.html")


@boozegame_server.route("/logout", methods=["GET"])
def logout():
    try:
        del global_team_register[current_user.team_id]
        logout_user()
    except AttributeError:  # User which is not logged in tries to log out
        pass
    except KeyError:  # Session expired before user loaded the page
        pass
    return login()


if __name__ == '__main__':
    global_team_register: dict = {}
    boozegame_server.run(host='0.0.0.0', port=5001, debug=True)

