# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information
import hashlib

from tinydb import TinyDB
from team import Team
from player import Player
from config import Config
from collections import Counter


class Database(TinyDB):
    def __init__(self, test_mode_enabled: bool = False):
        if not test_mode_enabled:
            super().__init__(Config.DATABASE_PATH)
        else:
            super().__init__(Config.TEST_MODE_DATABASE_PATH)

    def save_team(self, team_to_save: Team) -> None:
        if team_to_save.team_id is None:  # Team does not already exist
            team_to_save.team_id = self.insert({
                "name": team_to_save.name,
                "password": team_to_save.password,
                "players": [
                    player.to_save_state() for player in team_to_save.players
                ]
            })
        else:  # Team does already exist
            try:
                self.update({
                    "name": team_to_save.name,
                    "password": team_to_save.password,
                    "players": [
                        player.to_save_state() for player in team_to_save.players
                    ]
                }, doc_ids=[team_to_save.team_id])
            except KeyError:  # Team id could not been found in the database
                team_to_save.team_id = None
                self.save_team(team_to_save)

    def load_team(self, team_id: int) -> Team:
        team_data = self.get(doc_id=team_id)
        players: list[Player] = []
        for player in team_data["players"]:
            players.append(Player(player["name"], points=player["points"]))
        return Team(team_data["name"], players, team_id=team_id, password=team_data["password"])

    def get_scoreboard(self) -> list[tuple]:
        teams_with_score: dict[str, int] = {}
        for team in self.all():
            teams_with_score[team["name"]] = sum([p["points"] for p in team["players"]])

        return Counter(teams_with_score).most_common()  # Orders dict descending by score and transforms to list[tuple]


if __name__ == "__main__":
    test_team = Team("Test Team", [Player("Spieler1"), Player("Spieler2")], password="12345")
    with Database() as db:
        db.save_team(test_team)
