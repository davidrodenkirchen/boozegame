# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information

class Player:
    """
    The class Player represents a single player in Boozegame.
    The player is usually part of a team.
    """
    def __init__(self, name: str, points: int = 0):
        self.__name: str = name.strip()
        self.__points: int = points

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, new_name: str) -> None:
        self.__name = new_name

    @property
    def points(self) -> int:
        return self.__points

    @points.setter
    def points(self, new_point_total: int) -> None:
        self.__points = new_point_total

    def to_save_state(self) -> dict:
        """ returns a dict with all things that need to be saved """
        return {
            "name": self.__name,
            "points": self.__points
        }

    def add_points(self, points_to_add: int) -> None:
        self.__points += points_to_add

    def remove_points(self, points_to_remove: int) -> None:
        self.__points -= points_to_remove
        if self.__points < 0:  # Can not have negative points
            self.__points = 0
