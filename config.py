# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information

import os
from typing import AnyStr


class Config(object):
    LANGUAGES: list[str] = ['en', 'de']
    ROOT_DIR: AnyStr = os.path.dirname(os.path.abspath(__file__))
    DATABASE_PATH: bytes = os.path.join(ROOT_DIR, "database", "database.json")
    TEST_MODE_DATABASE_PATH: bytes = os.path.join(ROOT_DIR, "database", "test_database.json")
