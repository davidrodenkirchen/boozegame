# BoozeGame

An interactive web-based drinking game. Release date: not yet determined

## Description
Due to the PoC-Phase it is not yet suitable to publish a detailed description.

## Usage
Usage is provided through a deployed version on the internet.
If you want to run it locally (without a tracked team progress), you can use this snippet:
```shell
pip install -r requirements.txt
python3 boozegame_server.py
xdg-open http://localhost:5001
```
Note: You will need to register a new local account for yourself.

## Contributing
Contributions are not generally declined, mail to davidr.develop@gmail.com if you are interested.

## Contributions
Repository Icon: [FlatIcon](https://www.flaticon.com/de/kostenlose-icons/bier)

## License
See `LICENCE`

## Project status
Proof-of-Concept phase