# BoozeGame - An interactive web-based drinking game
# Copyright (C) 2022  David Rodenkirchen
# See `LICENCE` for full licence information
import hashlib
from typing import Union, Optional
from player import Player


class TeamHasDuplicateNamesException(Exception):
    """ Raised if a Team contains duplicate player names """
    pass


class Team:
    """
    The class Team represents a collection of players.
    A player can not be on its own and must always pe part of a team.
    """
    def __init__(self, team_name: str, players: Union[Player, list[Player]],
                 team_id: Optional[int] = None, password: str = None):
        self.__team_name: str = team_name.strip()
        players = players if isinstance(players, list) else [players]
        if self.__players_have_duplicate_names(players):
            raise TeamHasDuplicateNamesException
        self.__players: list[Player] = players
        self.__team_id: Optional[int] = team_id
        self.__password: str = password if password else str()  # It is expected to pass an already hashed value here

    @property
    def team_id(self) -> Optional[int]:
        return self.__team_id

    @team_id.setter
    def team_id(self, new_team_id: int) -> None:
        self.__team_id = new_team_id

    @property
    def name(self) -> str:
        return self.__team_name

    @name.setter
    def name(self, new_team_name: str) -> None:
        self.__team_name = new_team_name.strip()

    @property
    def password(self) -> str:
        return self.__password

    @password.setter
    def password(self, new_password: str) -> None:
        self.__password = hashlib.sha256(new_password.encode("utf-8")).hexdigest()

    @property
    def points(self) -> int:
        return sum([p.points for p in self.__players])

    @property
    def players(self) -> list[Player]:
        return self.__players

    def get_player_by_name(self, player_name: str) -> Optional[Player]:
        for player in self.__players:
            if player.name == player_name:
                return player

    def add_player(self, new_player: Union[Player, str]) -> None:
        if isinstance(new_player, str):
            new_player_name = new_player
            new_player = Player(new_player_name)
        future_player_list = self.__players.copy()
        future_player_list.append(new_player)
        if self.__players_have_duplicate_names(future_player_list):
            raise TeamHasDuplicateNamesException
        else:
            self.__players = future_player_list

    def remove_player(self, player_to_be_removed: Union[Player, str]) -> None:
        if isinstance(player_to_be_removed, str):
            player_to_be_removed = self.get_player_by_name(player_to_be_removed)
            if not player_to_be_removed:
                return
        new_player_list = list(filter(lambda p: p.name != player_to_be_removed.name, self.__players))
        self.__players = new_player_list

    ####################################
    # Start of methods for Flask-Login #
    ####################################

    def is_authenticated(self) -> bool:
        return True

    @staticmethod
    def is_active() -> bool:
        return True

    def get_id(self) -> str:
        return str(self.__team_id)

    @staticmethod
    def is_anonymous() -> bool:
        return False

    ##################################
    # End of methods for Flask-Login #
    ##################################

    @staticmethod
    def __players_have_duplicate_names(player_list) -> bool:
        player_names = [p.name for p in player_list]
        if len(player_names) != len(set(player_names)):
            return True
        return False

    def __len__(self):
        return len(self.__players)
